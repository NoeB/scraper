const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');

const url = 'url';

axios.get(url)
.then(res => 
{
    let getData = html =>
    {
        let data = [];
        const $ = cheerio.load(html);
        $('div.dzialka').each((i, elem) => 
        {
            data.push({
                id: $(elem).find('h3').text(),
                price: $(elem).find('p:nth-child(even)').text(),
                area: $(elem).find('p:nth-child(odd)').text(),
                sold: $(elem).find('img.sold').is('img'),
                img1: $(elem).find('div.imgContainer a:nth-child(odd)').attr('href'),
                img2: $(elem).find('div.imgContainer a:nth-child(even)').attr('href')
            });
        });

        fs.writeFile('warnik2.json', JSON.stringify(data, null, '\t'),  err =>
        {
            if(err) console.log('Error:', err);
            console.log('Data saved', data[0]);
        });
    }

    getData(res.data);
})
.catch(err => console.log(err));

